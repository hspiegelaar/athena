import boto3
import time

#Function for starting athena query
def run_query(query, database, s3_output):
    client = boto3.client('athena')
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={'Database': database},
        ResultConfiguration={'OutputLocation': s3_output}
        )
    print('Execution ID: ' + response['QueryExecutionId'])

    return response


s3_input = 's3://<my_bucket>/in'
s3_ouput = 's3://<my_bucket>/uit'
database = 'test_database'
table = 'persons'


#Query definitions
query_1 = 'SELECT * FROM {}.{} where sex = \'F\';'.format(database, table)
query_2 = 'SELECT * FROM {}.{} where age > 30;'.format(database, table)

#Execute all queries
queries = [ query_1, query_2 ]

for q in queries:

    # run the query
    print('\nExecuting query: {}'.format(q))
    res = run_query(q, database, s3_ouput)

    client = boto3.client('athena')
    print('\n === query execution')
    print(client.get_query_execution(QueryExecutionId=res['QueryExecutionId']))

    print('\n === query status')

    # request the status, wait until is is completed
    status = 'unknown'
    while status not in ['SUCCEEDED', 'FAILED', 'CANCELLED']:

        response = client.get_query_execution(QueryExecutionId=res['QueryExecutionId'])
        status = response['QueryExecution']['Status']['State']

        print(status)


    print('\n === query results')
    print(client.get_query_results(QueryExecutionId=res['QueryExecutionId'])) #['ResultSet']['Rows'])


'''
name string, sex string, city string, country string, age int, job string

{"name":"Alice","sex":"F","city":"Seattle","country":"USA","age":25,"job":"Professional Zombie Killer"}
{"name":"Bob","sex":"M","city":"Los Angeles","country":"USA","age":40,"job":"Actor Extraordinaire"}
{"name":"Joe","sex":"M","city":"New York","country":"USA","age":35,"job":"Policeman"}
{"name":"Amanda","sex":"F","city":"Los Angeles","country":"USA","age":29,"job":"Ex Child Star"}
'''